

// storing my dot position and computer dot position
var mypos = 1;
var compos = 1;




//event handlers for the menu panel buttons
document.getElementById('start').onclick = function(){
    mymove();   
}

document.getElementById('new').onclick = function(){
    mypos=1;
    compos=1;
    document.getElementById('mypos').innerHTML = "PLAYER "+mypos;
    document.getElementById('compos').innerHTML = "COMP "+compos;
    document.getElementById('msgtext').innerHTML = "";
    moveMyDot();
    moveCompDot();
}



// user defined functions for various tasks in the game


// Calculate move and make player move
function mymove()
{
    
    var newmove = Math.floor((Math.random()*6)+1);
    mypos = mypos + newmove;
     if(mypos>=100)
    {
        
        mypos=100;
        document.getElementById('mypos').innerHTML = "PLAYER "+mypos;
        moveMyDot();
        document.getElementById('msgtext').innerHTML = "You Win !!";
        $('#msgtext').show();
        return;
    }
    document.getElementById('mypos').innerHTML = "PLAYER "+mypos;
    moveMyDot();
    mypos = snakenladder(mypos);
    document.getElementById('mypos').innerHTML = "PLAYER "+mypos;
    moveMyDot();
    //setTimeout
    commove();
}



// Calculate move and make computer move
function commove()
{
    var newmove = Math.floor((Math.random()*6)+1);
    compos = compos + newmove;
    if(compos>=100)
    {
        compos=100;
        document.getElementById('compos').innerHTML = "COMP "+mypos;
        moveCompDot();
        document.getElementById('msgtext').innerHTML = "Computer Wins !!";
        $('#msgtext').show();
        return;
    }
    document.getElementById('compos').innerHTML = "COMP "+compos;
    moveCompDot();
    compos = snakenladder(compos);
    document.getElementById('compos').innerHTML = "COMP "+compos;
    moveCompDot();
    
}



// Check if current position has a snake or ladder effect
function snakenladder(pos)
{
    var elem = $('#msgtext');
    switch(pos)
    {
        case 8:
           // ladder
           pos = 55;
           elem.html("LADDER !!").show().fadeOut(3000);
           break;
        case 19:
            // ladder
           pos = 76;
            elem.html("LADDER !!").show().fadeOut(3000);
           break;
        case 32:
            // ladder
           pos = 92;
            elem.html("LADDER !!").show().fadeOut(3000);
           break;
        case 56:
            // snake
           pos = 4;
            elem.html("SNAKE !!").show().fadeOut(3000);
           break;
        case 88:
            // snake
           pos = 33;
           elem.html("SNAKE !!").show().fadeOut(3000);
           break;
        case 97:
            // snake
           pos = 59;
           elem.html("SNAKE !!").show().fadeOut(3000);
           break;
   
    }
    
    return pos;
}


// Creates the numbered grid 
function render()
{
    var grid = "";
    var i,j=1;var classnames = "";row="";
    var count = 100;
    for(i=1;i<=10;i++)
    {   row = "";
        for(j=1;j<=10;j++)
        {
            classnames = (count%2==0)?'grey':'green';
            classnames = classnames+' '+'gridbox'+' '+'left';
            var elem = "<div class='"+classnames+"'>"+count+"</div>";
            row = (i%2==0)?(elem+row):(row+elem);
            count--;
           
        }
           
            row = row + "<div class='clear'></div>";
            grid = grid+row;
    }
    
  
    document.getElementById('gridmask').innerHTML = document.getElementById('gridmask').innerHTML+grid;
}




// Moving the player dot to the new current position
function moveMyDot()
{   var offsetx = 0;
    var offsety = 0;
    var relpos = mypos%10;
    var band = Math.floor(mypos/10);
    
    if(relpos==0)
    {    
                
                if(band%2==0)
            {
                offsetx = 0;
               
            }
            else
            {
                 offsetx = 81;
            }
                band = (band==0)?0:band-1;
                offsety = 81-((band)*9);
    }
    else{
                if(band%2==0)
            {
                offsetx = ((relpos-1)*9);
               
            }
            else
            {
                 offsetx = (81-((relpos-1)*9));
            }
                offsety = 81-((band)*9);
    }

    // Animating the dot using jquery 
    $('#mydot').animate({"left":offsetx+"%","top":offsety+"%"},"slow");
    
    
}


// Moving the computer dot to the new current position
function moveCompDot()
{
    var offsetx = 0;
    var offsety = 0;
    var relpos = compos%10;
    var band = Math.floor(compos/10);
   
     if(relpos==0)
    {    
                
                if(band%2==0)
            {
                offsetx = 0;
               
            }
            else
            {
                 offsetx = 81;
            }
                band = (band==0)?0:band-1;
                offsety = 81-((band)*9);
    }
    else{
                if(band%2==0)
            {
                offsetx = ((relpos-1)*9);
               
            }
            else
            {
                 offsetx = (81-((relpos-1)*9));
            }
                offsety = 81-((band)*9);
    }
    
      // Animating the dot using 
      $('#comdot').animate({"left":offsetx+"%","top":offsety+"%"},"slow");
}


$(function(){
    
    
    // snake and ladders using HTML, CSS, Javascript and a bit of Jquery !!
  
    
    })