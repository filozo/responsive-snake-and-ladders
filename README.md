# README #



### What is this repository for? ###

* A responsive Snake and Ladders using HTML, CSS, Javascript and bit of Jquery.


Specifications - 


1. The images have been optimised for smartphone browsers.
2. The css is responsive.
3. There are no frameworks used apart from Jquery, which in turn provides a smoothing effect to the animation.
4. Its a single player game played against the computer.


Regards
Satyam
satyam.nitt@gmail.com